import tornado.ioloop
import tornado.httpserver
import pyrestful.rest
from handlers.search_handler import SearchService
from logger import logger
app = pyrestful.rest.RestService([
    SearchService
])


def runserver():
    server = tornado.httpserver.HTTPServer(app)
    server.listen(9999)
    logger.info("Server has been stared with {ip}:{port}".format(ip="127.0.0.1", port=9999))
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    runserver()