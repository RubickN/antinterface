import pyrestful.rest
import uuid
import time
import tornado.ioloop
import tornado.web
import traceback
from concurrent.futures import ThreadPoolExecutor
from ant_interface.logger import logger
from functools import wraps, partial
EXECUTOR = ThreadPoolExecutor(max_workers=64)


class ReturnData(object):
    def __init__(self, code=200, message='', data=None, total=-1):
        self.code, self.message, self.data, self.total = code, message, data, total

    @property
    def value(self):
        return_map = {
            "meta": {
                "code": self.code,
                "message": self.message,
                **({'total': self.total} if self.total != -1 else {})
            },
            "data": self.data
        }
        return return_map


def unblock(f):
    """
    :param f:
    :return:
    """
    @tornado.web.asynchronous
    @wraps(f)
    def wrapper(*args, **kwargs):
        self = args[0]

        def callback(future):
            try:
                self.on_return(future.result())
            except Exception:
                print(traceback.format_exc())
                pass

        if EXECUTOR._work_queue.qsize() == 0:
            EXECUTOR.submit(
                partial(f, *args, **kwargs)
            ).add_done_callback(
                lambda future: tornado.ioloop.IOLoop.instance().add_callback(
                    partial(callback, future)
                )
            )
        else:
            self.set_status(503)
            self.on_return(ReturnData(503, "服务器繁忙"))
    return wrapper


class BaseHandler(pyrestful.rest.RestHandler):
    def initialize(self):
        self.logger = logger
        self.uuid = uuid.uuid4().hex
        self.stime = time.time()
        self.logger.info("[{0}] {1}({2}): {3}".format(
            self.uuid,
            self.request.method,
            self.request.remote_ip,
            self.request.uri
        ))

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")  # 这个地方可以写域名
        self.set_header("Access-Control-Allow-Headers", "Content-Type, x-requested-with")
        self.set_header('Access-Control-Alsslow-Methods', 'POST, GET')

    def on_return(self, return_data):
        if isinstance(return_data, ReturnData):
            self.write(return_data.value)
            self.finish()
            self.logger.info("Handler end", extra={
                "uuid": self.uuid,
                "method": self.request.method,
                "code": return_data.code,
                "remote_ip": self.request.remote_ip,
                "url": self.request.uri,
                "usetime": time.time() - self.stime
            })
        else:
            self.write(return_data)
            self.finish()
            self.logger.info("Handler end", extra={
                "uuid": self.uuid,
                "method": self.request.method,
                "remote_ip": self.request.remote_ip,
                "url": self.request.url,
                "usertime": time.time() - self.stime
            })