from ServiceInterface.handlers.base import BaseHandler, unblock, ReturnData
from pyrestful.rest import get
from aghanim import get_config, AghanimInit
from stream_handler import FileStreamHandler
from const import BaseConfiguration
from total_engine import TotalEngine
from collections import defaultdict
import math


def aghanim_factor():
    # 获取配置
    configs = get_config()
    # 初始化aghanim
    aghanim = AghanimInit()
    # 如果需要清除缓存
    if configs['is_update_resources'] == 1:
        aghanim.clean_cache()
    aghanim.register_output_stream(FileStreamHandler(BaseConfiguration.RESOURCE_FILE,
                                                     "{date}-{title}_{id}.{file_type}"))
    aghanim.register_result_stream(FileStreamHandler(BaseConfiguration.RESULT_HTML_DIR,
                                                     "{date}-{title}_{resource_id}"))
    return aghanim


aghanim = aghanim_factor()


class SearchService(BaseHandler):

    @get(_path="/searcher/hello_world")
    @unblock
    def hello_world(self):
        return ReturnData(200, "hello world")

    @get(_path="/searcher/resource_num")
    @unblock
    def get_resource_num(self):
        return ReturnData(200, "success", {"data": aghanim.resource_num})

    @get(_path="/searcher/latest_date")
    @unblock
    def get_latest_date(self):
        return ReturnData(200, "success", {"data": aghanim.latest_resource_date})

    @get(_path="/searcher/search_summary")
    @unblock
    def get_search_summary(self):
        mapping = defaultdict(lambda x: TotalEngine.YEAR_MONTH_DAY)
        mapping.update({
            "111": TotalEngine.YEAR_MONTH_DAY,
            "110": TotalEngine.YEAR_MONTH,
            "100": TotalEngine.YEAR,
            "101": TotalEngine.YEAR_DAY,
            "011": TotalEngine.MONTH_DAY,
            "010": TotalEngine.MONTH,
            "001": TotalEngine.DAY

        })
        key_word = self.get_argument('key_word')
        nums = int(self.get_argument('nums', 10))
        sort = self.get_argument('sort', 'asc')
        page = int(self.get_argument('page', 1))
        date_type = self.get_argument('chart_type', "110")
        target_date = self.get_argument('target_date', 'none')
        chart_type = mapping[date_type]
        results = aghanim.search_words_without_load(key_word)
        total_engine = TotalEngine([result['date'] for result in results])
        is_reversed = False
        if sort == 'asc':
            is_reversed = True
        results = sorted(results, key=lambda x: int(x['resource_id']), reverse=is_reversed)
        if target_date not in ['none']:
            results = list(filter(lambda x: x['date'].startswith(target_date), results))
        total = len(results)
        start = (page - 1) * nums
        end = start + nums
        total_page = math.ceil(float(total) / nums)
        data = [[i['title'], i['resource_id'], i['date']] for i in results[start: end]]
        total_result = total_engine.total(chart_type)
        return ReturnData(200, "success", {"data": data,
                                           "total": total,
                                           "start": start,
                                           "end": end,
                                           "current_page": page,
                                           "total_page": total_page,
                                           "x_value": list(total_result.index),
                                           "y_value": list(total_result["count"])})

    def _search(self, key_word, resource_id):
        results = aghanim.search_words_without_load(key_word)
        data = {i['resource_id']: i['content'] for i in results if i['resource_id'] == resource_id}
        return data

    @get(_path="/searcher/search_html")
    # @unblock
    def get_search_html(self):
        key_word = self.get_argument('key_word')
        resource_id = self.get_argument('resource_id')
        results = aghanim.search_words_without_load(key_word)
        data = {i['resource_id']: i['content'] for i in results if i['resource_id'] == resource_id}
        self.write(BaseConfiguration.CSS_STYLE + "<" + data[resource_id])

    @get(_path="/searcher/search")
    @unblock
    def get_saerch(self):
        key_word = self.get_argument('key_word')
        resource_id = self.get_argument('resource_id')
        data = self._search(key_word, resource_id)
        return ReturnData(data)

    @get(_path="/searcher/common_word/save")
    @unblock
    def save_common_word(self):
        key_word = self.get_argument('key_word')
        aghanim.register_common_word(key_word)
        return ReturnData(200, "保存成功")

    @get(_path="/searcher/common_word/delete")
    @unblock
    def delete_common_word(self):
        key_word = self.get_argument('key_word')
        aghanim.delete_common_word(key_word)
        return ReturnData(200, "删除成功")

    @get(_path="/searcher/common_word")
    @unblock
    def get_common_word(self):
        data = aghanim.get_common_word()
        return ReturnData(200, "获取数据成功", {'data': data})



