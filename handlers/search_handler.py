from .base import BaseHandler, unblock, ReturnData
from pyrestful.rest import get
from aghanim.aghanim import CenterManager
from collections import defaultdict
import re
import pandas
import math
manager = CenterManager()


class TotalEngine(object):
    YEAR_MONTH_DAY = 'year_month_date'
    MONTH_DAY = 'month_date'
    YEAR_MONTH = 'year_month'
    YEAR_DAY = 'year_day'
    MONTH = 'month'
    YEAR = 'year'
    DAY = 'day'

    def __init__(self, result_list):
        self.result_list = result_list
        self._date_list = [self._date_format(_date_str) for _date_str in self.result_list]

    def _date_format(self, _str):
        pattern = re.compile("^(\d+)-(\d+)-(\d+)")
        mm = re.search(pattern, _str)
        if mm:
            return mm.groups()[0], mm.groups()[1], mm.groups()[2], _str
        return 1900, 1, 1

    @property
    def date_tuple(self):
        return self._date_list

    @property
    def date_year_list(self):
        return [_date[0] for _date in self.date_tuple]

    @property
    def date_month_list(self):
        return [_date[1] for _date in self.date_tuple]

    @property
    def date_day_list(self):
        return [_date[2] for _date in self.date_tuple]

    @property
    def date_year_month_list(self):
        return [_date[0] + '-' + _date[1] for _date in self.date_tuple]

    @property
    def date_month_day_list(self):
        return [_date[1] + '-' + _date[2] for _date in self.date_tuple]

    @property
    def date_year_month_day_list(self):
        return ['-'.join(_date[0:3]) for _date in self.date_tuple]

    @property
    def date_year_day_list(self):
        return [_date[0] + '-' + _date[2] for _date in self.date_tuple]

    @property
    def file_list(self):
        return [_date[3] for _date in self.date_tuple]

    def total(self, _type):
        if _type == self.YEAR_MONTH:
            data_list = self.date_year_month_list
        elif _type == self.YEAR:
            data_list = self.date_year_list
        elif _type == self.MONTH:
            data_list = self.date_month_list
        elif _type == self.DAY:
            data_list = self.date_day_list

        elif _type == self.MONTH_DAY:
            data_list = self.date_month_day_list
        elif _type == self.YEAR_DAY:
            data_list = self.date_year_day_list
        elif _type == self.YEAR_MONTH_DAY:
            data_list = self.date_year_month_day_list
        else:
            raise NotImplemented

        _tmp = pandas.DataFrame({_type: data_list,
                                 'count': [1 for d in data_list]})
        tmp_result = _tmp.groupby(_type).count()
        return tmp_result

    @property
    def month_day(self):
        return self.total(self.MONTH_DAY)

    @property
    def year_month(self):
        return self.total(self.YEAR_MONTH)

    @property
    def year_month_day(self):
        return self.total(self.YEAR_MONTH_DAY)

    @property
    def years(self):
        return self.total(self.YEAR)

    @property
    def months(self):
        return self.total(self.MONTh)


class SearchService(BaseHandler):

    @get(_path="/searcher/hello_world")
    @unblock
    def hello_world(self):
        return ReturnData(200, "hello world")

    @get(_path="/searcher/resource_num")
    @unblock
    def get_resource_num(self):
        return ReturnData(200, "success", {"data": manager.get_all_resource_nums()})

    @get(_path="/searcher/latest_date")
    @unblock
    def get_latest_date(self):
        return ReturnData(200, "success", {"data": manager.get_latest_date()})

    @get(_path="/searcher/search_summary")
    @unblock
    def get_search_summary(self):
        mapping = defaultdict(lambda x: TotalEngine.YEAR_MONTH_DAY)
        mapping.update({
            "111": TotalEngine.YEAR_MONTH_DAY,
            "110": TotalEngine.YEAR_MONTH,
            "100": TotalEngine.YEAR,
            "101": TotalEngine.YEAR_DAY,
            "011": TotalEngine.MONTH_DAY,
            "010": TotalEngine.MONTH,
            "001": TotalEngine.DAY

        })
        key_word = self.get_argument('key_word')
        nums = int(self.get_argument('nums', 10))
        sort = self.get_argument('sort', 'asc')
        page = int(self.get_argument('page', 1))
        date_type = self.get_argument('chart_type', "110")
        target_date = self.get_argument('target_date', 'none')

        chart_type = mapping[date_type]
        results = manager.search_all(key_word)
        total_engine = TotalEngine([result.date for result in results])
        is_reversed = False
        if sort == 'asc':
            is_reversed = True
        results = sorted(results, key=lambda x: int(x.id), reverse=is_reversed)
        if target_date not in ['none']:
            results = list(filter(lambda x: x.date.startswith(target_date), results))
        total = len(results)
        start = (page - 1) * nums
        end = start + nums
        total_page = math.ceil(float(total) / nums)
        data = [[i.title, i.id, i.date] for i in results[start: end]]
        total_result = total_engine.total(chart_type)
        return ReturnData(200, "success", {"data": data,
                                           "total": total,
                                           "start": start,
                                           "end": end,
                                           "current_page": page,
                                           "total_page": total_page,
                                           "x_value": list(total_result.index),
                                           "y_value": list(total_result["count"])})

    @get(_path="/searcher/search_html")
    # @unblock
    def get_search_html(self):
        key_word = self.get_argument('key_word')
        resource_id = self.get_argument('resource_id')
        self.write(self._search(key_word, resource_id))

    @get(_path="/searcher/search")
    @unblock
    def get_saerch(self):
        key_word = self.get_argument('key_word')
        resource_id = self.get_argument('resource_id')
        data = self._search(key_word, resource_id)
        return ReturnData(data)

    def _search(self, key_word, resource_id):
        results = manager.search_all(key_word)
        data = {i.id: i.content for i in results if i.id == resource_id}
        return data